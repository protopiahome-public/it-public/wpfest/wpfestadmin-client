export default class User
{
	static display_name = "";
	static roles = [];
	static setData(data)
	{
		User.display_name	= data.display_name;
		User.roles			= data.roles;
	}
	static deleteData()
	{
		User.display_name	= "";
		User.roles			= [];
	}
}