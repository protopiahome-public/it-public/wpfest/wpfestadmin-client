import React, {Component, Fragment} from "react";
import { 
	Tab, Tabs, 
	Button, 
	Card, 
	Elevation,    
	Icon,
    InputGroup,
    Intent,
	FormGroup, ButtonGroup,
	Position, Popover
} from "@blueprintjs/core";
import { __ } from "../utilities/Voc";


export default class RegistrationForm extends Component
{
	state = {
		lgn:"",
		psw:"",
		psw2:"",
		first_name:"",
		last_name:"",
		email:"",		
	}
	onLogout =() =>
	{
		this.props.onLogout();
	}
	fullBool = () =>
	{
		return this.state.lgn == "" 
			|| this.state.psw != this.state.psw2 
			|| this.state.psw == "" 
			|| this.state.first_name == ""
			|| this.state.last_name == "";
	}
	onRegister = () =>
	{
		if( this.fullBool() ) 
		{
			return;
		}
		this.props.onRegister(this.state);
	}
	onLogin = evt =>
	{
		this.setState({lgn : evt.currentTarget.value});
	}
	onFirstName = evt =>
	{
		this.setState({first_name : evt.currentTarget.value});
	}
	onLastName = evt =>
	{
		this.setState({last_name : evt.currentTarget.value});
	}
	onPassword = evt =>
	{
		this.setState({psw : evt.currentTarget.value});
	}
	onPassword2 = evt =>
	{
		this.setState({psw2 : evt.currentTarget.value});
	}
	render()
	{		
		return <section className="m">
			<div className="container d-flex justify-content-center align-items-center node m" >
				<Card interactive={true} elevation={Elevation.TWO} style={{width:"100%", marginTop:40}}>
					<div className="float-right">
						<Button icon="cross" onClick={this.onLogout} />
					</div>
					<div className="display-4 my-5">
						{ __("Registration") }
					</div>
					<FormGroup
						intent={ this.state.lgn == "" ? Intent.DANGER : Intent.NONE  }
						requiredLabel={true}
						label={__("login")}
						helperText="one word, only latins and numbers"
						className="p-2"
					>
						<InputGroup  
							intent={ this.state.lgn == "" ? Intent.DANGER : Intent.NONE  }
							leftIcon={"follower"}
							placeholder={__("set login")}
							fill={true}  
							large={true}  
							value={this.state.lgn}
							onChange={ this.onLogin}
						/>
					</FormGroup>
					<FormGroup
						intent={ this.state.first_name == "" ? Intent.DANGER : Intent.NONE  }
						requiredLabel={true}
						label={__("first name")}
						helperText=" "
						className="p-2"
					>
						<InputGroup  
							intent={ this.state.first_name == "" ? Intent.DANGER : Intent.NONE  }
							leftIcon={"follower"}
							placeholder={__("set first name")}
							fill={true}  
							large={true}  
							value={this.state.first_name}
							onChange={ this.onFirstName}
						/>
					</FormGroup>
					<FormGroup
						intent={ this.state.last_name == "" ? Intent.DANGER : Intent.NONE  }
						requiredLabel={true}
						label={__("first name")}
						helperText=" "
						className="p-2"
					>
						<InputGroup  
							intent={ this.state.last_name == "" ? Intent.DANGER : Intent.NONE  }
							leftIcon={"follower"}
							placeholder={__("set first name")}
							fill={true}  
							large={true}  
							value={this.state.last_name}
							onChange={ this.onLastName}
						/>
					</FormGroup>
					<FormGroup
						intent={ this.state.psw != this.state.psw2 || this.state.psw == "" ? Intent.DANGER : Intent.NONE }
						requiredLabel={true}
						label={__("password")}
						helperText="one word, only latins and numbers"
						className="p-2"
					>
						<InputGroup 
							intent={ this.state.psw != this.state.psw2 || this.state.psw == "" ? Intent.DANGER : Intent.NONE } 
							leftIcon={"eye-off"}
							fill={true}  
							large={true}  
							value={this.state.psw}
							onChange={ this.onPassword}
							type="password"
							placeholder={__("set password")}
						/>
						<div className="p-1"/>
						<InputGroup  
							intent={ this.state.psw != this.state.psw2 || this.state.psw2 == "" ? Intent.DANGER : Intent.NONE }
							leftIcon={"eye-off"}
							fill={true}  
							large={true}  
							value={this.state.psw2}
							onChange={ this.onPassword2}
							placeholder={__("repeat password")}
							type="password"
						/>
					</FormGroup>
					<Popover
						position={Position.TOP_LEFT}
						content={
							<div className="square p-3">
								<div className="lead text-center">
									{__( 
										!this.fullBool()
											? "Are you realy want create account?"
											: "No all parameters are selected!"
									)}
								</div>
								{
									!this.fullBool()
										? <ButtonGroup className="p-2 tex-right">
											<Button
												intent={Intent.DANGER}
												text={__("Yes")}
												onClick={this.onRegister}
											/>
										</ButtonGroup> 
										: 
										<div className="py-3">
											<Icon icon="error" iconSize={30} />
										</div>
								}
							</div>
						}
					>
						<Button text={__("Register")} rightIcon="person" />
					</Popover>
				</Card>
			</div>
		</section>
	}
}