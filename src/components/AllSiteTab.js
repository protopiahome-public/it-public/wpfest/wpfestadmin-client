import React, {Component, Fragment} from "react";
import {__} from "../utilities/Voc";
import SiteSingle from "./page/SiteSingle";
import NewSiteForm from "./page/NewSiteForm";
import _fetch from "../api";
import {AppToaster} from "../utilities/blueUtils";
import Pagi from "../utilities/Pagi";
import {  
	Icon, 
	Intent, 
	InputGroup,
	Card, 
	Elevation, 
	Button, 
	ButtonGroup, 
	TextArea, 
	Tooltip,
	Position,
	PopoverInteractionKind, 
	Popover,
	MenuItem, FormGroup
} from "@blueprintjs/core";

export default class PatternSiteCard extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			state:0,
			domain:"",
			blog_id:-1,
			isNewForm:false,
			post_title:"",
			post_content:"",
			all_sites_count: 0,
			all_sites_number: 100,
			all_sites_offset: 0,
			all_sites: [],
		}
	}
	componentWillReceiveProps( nextProps )
	{
		//console.log(nextProps);
		this.setState({
			all_sites_count: nextProps.all_sites_count,
			all_sites_number: nextProps.all_sites_number,
			all_sites_offset: nextProps.all_sites_offset,
			all_sites: nextProps.all_sites,
		});
	}
	render()
	{
		let cont;
		switch(this.state.state)
		{
			case 1:
				cont = this.state1();
				break;
			case 0:
			default:
				cont = this.state0();
		}
		return <div className="card w-100 p-0 node">
			{cont}
		</div>;
	}
	state0()
	{
		const {sites } = this.props;
		const { all_sites_count, all_sites_number, all_sites_offset, all_sites} = this.state;
		const siteList = all_sites.map(elem => <li className="list-group-item" key={elem.domain}>
			<div 
				className="row pointer tex-sm-left text-center"
				domain={elem.domain}
				blog_id={elem.blog_id}
				onClick={this.onSite} 
			>
				<div className="col-md-2 col-12">{elem.blog_id}</div>
				<div 
					className="col-md-5 col-12" 
				>
					{elem.domain}
				</div>
				<div className="col-md-5">
					{ elem.domain_type ? <span className="member_span">{elem.domain_type}</span> : null }
				</div>
			</div>
		</li>);
		const newForm = 	this.state.isNewForm ?  
		<li className="list-group-item p-0">
			<NewSiteForm
				sites={sites}
				onClose={this.openNew}
				unload={this.unload}
			/>
		</li>
		: 
		<Fragment>
			<ul className="list-group">
				{siteList}		
					
			</ul>
			<div className="row">
				<div className=" col-sm-7">
					<Pagi
						all={Math.ceil( all_sites_count / all_sites_number )}
						current={ all_sites_offset  }
						onChoose={ this.onPagi }
					/> 
				</div>
				<div className="col-sm-5 text-right" style={{display:this.state.isNewForm  ? "none" : "inline-block"}}>
					<Button text={__("create new Festival")} onClick={this.openNew} fill={true} minimal={true}/>
				</div>
			</div>
		</Fragment>
		return <Fragment> 
			{newForm}
		</Fragment>
	}
	state1()
	{
		return <Fragment>
			<SiteSingle blog_id={this.state.blog_id} onList={this.onList} domain={this.state.domain}/>
		</Fragment>
	}
	onPagi = n =>
	{
		this.props.onPagi(n);
	}
	onSite = evt =>
	{
		const blog_id = evt.currentTarget.getAttribute("blog_id");
		const domain = evt.currentTarget.getAttribute("domain");
		//console.log(blog_id);
		this.setState({state:1, blog_id, domain});
	}
	onList = () =>
	{
		this.setState({state:0});
	}
	openNew = () =>
	{
		this.setState({isNewForm:!this.state.isNewForm});
	}
	unload = sites =>
	{
		this.setState({isNewForm:false, all_sites:sites});
	}
}