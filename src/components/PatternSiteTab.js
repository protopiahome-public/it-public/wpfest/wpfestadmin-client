import React, {Component} from "react";
import {__} from "../utilities/Voc";
import { Tab, Tabs, Button, ButtonGroup } from "@blueprintjs/core";
import PatternSiteCard from "./PatternSiteCard";
//import sites from "../data/sites.json";

export default class PatternSiteTab extends Component
{
	constructor(props)
	{
		super(props);
		this.state={
			navbarTabId:props.current,
			sites:props.sites || []
		}
	}
	componentWillReceiveProps( nextProps )
	{
		//console.log(nextProps);
		this.setState({ navbarTabId : nextProps.current });
	}
	render()
	{
		let cards = this.props.sites.map((elem, i)=>
			<Tab 
				id={"site" +i} 
				title={ elem.title} 
				panel={ <PatternSiteCard
					title={elem.title}
					domain={elem.domain}
					description={elem.description}
					id={elem.id}
					num={i}
					onTitle={this.onTitle}
					onDomain={this.onDomain}
					onContent={this.onContent}
					onRemove={this.removeTab}
				/> } 
				key={elem.id}
				className="col-12 " 
			/>
		);
		cards.push( <Tabs.Expander key={"ext"}/> );
		cards.push( <ButtonGroup large={false} key={"add"} className="mt-5">
			<Button icon="plus" text={__("Add")} onClick={this.addTab}/ >
			<Button icon="floppy-disk" text={__("Save")} onClick={this.onSave} />
			
		</ButtonGroup> );

		return <div>
			<div className="row">
				<div className="col-12">
					 <Tabs
						animate={true}
						id="Tabss"
						vertical={true}
						selectedTabId={this.state.selectedTabId}
						onClick={this.onClick}
					>
						{cards}
					</Tabs>
				</div>
			</div>
		</div>
	}
	onClick = navbarTabId =>
	{
		 this.setState({ navbarTabId })
	}
	onSave = () =>
	{
		this.props.onSave();
	}
	removeTab = num =>
	{
		this.props.removeTab( num );
	}
	addTab = () =>
	{
		this.props.addTab();
	}
	onTitle = (title, num) =>
	{
		this.props.onTitle(title, num, "title");
	}
	onDomain = (title, num) =>
	{
		this.props.onTitle(title, num, "domain");
	}
	onContent = (title, num) =>
	{
		this.props.onTitle(title, num, "description");
	}
}