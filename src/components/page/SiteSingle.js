import React, {Component} from "react";
import _fetch from "../../api";
import ButtonList from "./ButtonList";
import buttons from "./buttons.json";
import Panel from "./Panel";
import {__} from "../../utilities/Voc";

import {  
	Icon, 
	Intent, 
	InputGroup,
	Card, 
	Elevation, 
	Button, 
	ButtonGroup, 
	TextArea, 
	Tooltip,
	Position,
	PopoverInteractionKind, 
	Popover
} from "@blueprintjs/core";

export default class SiteSingle extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			active:0,
			active2:0,
			currentButton:buttons[0],
			data:{},
			domain:"",
			url:"",
			description:"",
			icon:""
		}
	}
	componentWillMount() 
	{
		//console.log(this.props.blog_id);
		_fetch("site", {id:this.props.blog_id})
			.then(data =>
			{
				console.log( data );
				this.setState({
					icon		: data.site.icon,
					url			: data.site.url,
					domain		: data.site.domain,
					blog_id		: data.site.blog_id,
					description	: data.site.description,
					data		: data.site.options
				});
			});
	}
	render()
	{
		const {icon, domain, blog_id, description} = this.state;
		const children = buttons[this.state.active].children ?
			buttons[this.state.active].children.map((elem, i) =>
			{
				return <div 
					style={{
						//height:130, 
						backgroundColor:this.state.active2 == i ? "white": "rgb(255, 110, 74)",
						color:this.state.active2 != i ? "white": "rgb(32, 43, 51)",
						marginBottom:10,
						marginLeft:-5,
						marginRight:-5,
						cursor:"pointer",
						animationName: "wobble3",
						animationDuration: ".75s",
						animationDelay: ( .25 * i ) + "s",
						animationFillMode: "both",
						opacity:0
					}}
					className="uabtn"
					key={elem.component}
					n={i}
					onClick={this.onChildren}
				>
					<Icon icon={elem.icon}  iconSize={20} /> <div className="small mt-sm-1">{__(elem.title)}</div>
				</div>
			}) : null;
		return <Card interactive={false} className="w-100 py-0 node"  style={{paddingRight:0}}>
			<div className="row node">
				<div className="col-sm-2 fl uabtn-cont col-6 order-1" style={{ backgroundColor:"#202B33" }}>
					<ButtonList active={this.state.active} switchPanel={this.switchPanel} />
				</div>
				<div className="col-sm-8 col-12 order-sm-2 order-3">
					<Panel 
						active={this.state.active} 
						currentButton={this.state.currentButton} 
						data={this.state.data || { format:"", termin:"" }} 
						blog_id={this.props.blog_id} 
						url={this.state.url}
					/>
				</div>
				<div className="col-sm-2 fl uabtn-cont col-6 order-sm-3 order-2" style={{ backgroundColor:"#30404D" }}>
					<div 
						className={"uabtn pointer"}
						style={{ margin: "0 -15px 0 -15px", textAlign:"center", color:"#FF6E4A" }}
						onClick={this.props.onList}
					>
						<Icon icon={"undo"}  iconSize={20} /> <div className="small mt-1">{__("to list")}</div>
					</div>
					<div 
						className={"text-dark bg-light uabtn hidden"}
						style={{ margin: "0 -15px 0 -15px", textAlign:"center" }}						
					>
						<div className="">
							<img src={ icon } alt=""  height={40}/>
						</div>
						<div className="small mt-1">{this.props.domain}</div>
					</div>
					{ children }
				</div>
			</div>
		</Card>
	}
	onChildren = evt =>
	{
		const n = parseInt(evt.currentTarget.getAttribute("n"));
		this.switchPanel2(n);
	}
	switchPanel2 = ( n ) =>
	{
		_fetch( buttons[this.state.active].children[n].command, {id:this.state.blog_id} ).
			then(data =>
			{
				//console.log(data);
				//console.log(buttons[n].command);
				this.setState({ 
					active2:n, 
					currentButton:buttons[this.state.active].children[n],
					data:data.lead 
				});
			});
		
	}
	switchPanel = ( n ) =>
	{
		_fetch( buttons[n].command, {id:this.state.blog_id} ).
			then(data =>
			{
				//console.log(data);
				//console.log(buttons[n].command);
				this.setState({ 
					active:n, 
					active2:0, 
					currentButton:buttons[n],
					data:data.lead 
				});
			});
		
	}
}