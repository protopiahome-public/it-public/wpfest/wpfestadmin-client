import React, {Component, Fragment} from "react";
import {__} from "../../utilities/Voc";
import _fetch from "../../api";
import Dropdown from "../../utilities/Dropdown";
import {AppToaster} from "../../utilities/blueUtils";
import {  
	Icon, 
	Intent, 
	InputGroup,
	Card, 
	Elevation, 
	Button, 
	ButtonGroup, 
	TextArea, 
	Tooltip,
	Position,
	PopoverInteractionKind, 
	Popover,
	MenuItem, FormGroup, ProgressBar
} from "@blueprintjs/core";
import { Map, Marker, MarkerLayout } from 'yandex-map-react'; //https://github.com/effrenus/yandex-map-react
import DateTimeForm from "../cabinet/single/DateTimeForm";

export default class NewSiteForm extends Component
{
	state = { 
		post_title : "", 
		post_content : "", 
		site : this.props.sites[0].domain_id, 
		domain:"", 
		state:"form", 
		number:100,
		markers:[]
	};
	
	
	render()
	{
		switch(this.state.state)
		{
			case "loading":
				return this.loading();
			case "form":
			default:
				return this.form();
		}
	}
	loading()
	{
		return <Card className="p-4 w-100" interactive={true}>
			<div style={{width:200, margin:"120px auto"}}>
				<ProgressBar className="new" intent={Intent.NONE} value={null} />
			</div>
		</Card>
	}
	form()
	{
		//console.log(this.props.sites);
		return <Card className="p-4 w-100" interactive={true}>
			<div className="float-right p-0">
				<Button icon="cross" onClick={this.onClose} />
			</div>
			<div className="lead mb-2">{__("Create new Festival")}</div>
			<FormGroup
				label={__("Title")}
				helperText="insert title by any symbols"
				className="p-2"
			>
				<input 
					type="text"
					className="form-control"
					value={this.state.post_title}
					onChange={ this.onTitle}
				/>
			</FormGroup>
			<FormGroup
				label={__("Type of Festival")}
				helperText="Choose example copied"
				className="p-2"
			>
				<Dropdown
					options={this.props.sites.map(e => {
						return { post_title: e.title, descr: e.description, id: e.domain_id }
					})}
					multiple={false}
					onClick={this.onSite}
					current={this.state.site}
					className="btn-outline-secondary"
				/>
			</FormGroup>
			<FormGroup
				label={__("New Site - Address")}
				helperText="Only lowercase letters (a-z) and numbers are allowed."
				className="p-2"
			>
				<input 
					type="text"
					className="form-control"
					value={this.state.domain}
					onChange={ this.onDomain}
					pattern={"[^a-z0-9]+"}
				/>
			</FormGroup>
			<FormGroup
				label={__("Put Date")}
				helperText="Start date of Event"
				className="p-2"
			>
				<DateTimeForm onChange={this.onDate} />
			</FormGroup>
			<FormGroup
				label={__("Put geoposition")}
				helperText="Set place of Event"
				className="p-2"
			>
				<Map 
					onAPIAvailable={function () { console.log('API loaded'); }} 
					onClick={this.onMapClick}
					center={[55.754734, 37.583314]} 
					zoom={10}
					width="100%"
					height={300}
				>
				{					
					this.state.markers[0] 
						?
						<Marker 
							lat={ this.state.markers[0] } 
							lon={ this.state.markers[1] } 
						/>
						:
						null
				}
				</Map>
			</FormGroup>
			<Popover 
				position={Position.TOP_LEFT}
				content={ this.cont() }
			>
				<Button
					text={__("Create")}
				/>
			</Popover >
		</Card>
	}
	onMapClick = pars => this.setState({markers: pars.get('coords')}); 
	onClose = () => this.props.onClose();
	isSeq = () =>
	{
		return this.state.post_title != "" && this.state.domain != "";
	}
	cont = () =>
	{
		return this.isSeq() 
			? 
			<div className="square">
				<div className="mb-3">
					{__("Do you really want create Festival?")}
				</div>
				<ButtonGroup>
					<Button
						intent={Intent.DANGER}
						text="Yes"
						onClick={this.onCreate}
					/>
				</ButtonGroup>
			</div> 
			:
			<div className="square">
				<div className="lead">
					{__("All fields must fill!")}
				</div>
			</div>
	}
	onTitle = evt =>
	{
		const value = evt.currentTarget.value;
		let style = Object.assign(this.state, {});
		style.post_title	= value;
		this.setState( style );
	}
	onDomain = evt =>
	{
		const value = evt.currentTarget.value.replace(/[^a-z0-9]+/gi, "");
		this.setState({ domain : value });
	}
	onSite = n =>
	{
		this.setState({ site : n });
	}
	onCreate =() =>
	{
		console.log(this.state);
		//	return;
		this.setState({state:"loading"});
		_fetch( "create_site", this.state )
			.then(data =>
			{
				console.log(data);
				this.setState({ state:"form" });
				this.props.unload(data.sites || []);
			});
	}
	onDate = date =>
	{
		this.setState({ date : date });
		console.log(date);
	}
	replacer= text =>
	{
		const r = {
			а : "a",
			б : "b",
			в : "v",
			г : "g",
			д : "d",
			е : "e",
			ё : "yo",
			ж : "j",
			з : "z",
			и : "i",
			й : "y",
			к : "k",
			л : "l",
			м : "m",
			н : "n",
			о : "o",
			п : "p",
			р : "r",
			с : "s",
			т : "t",
			у : "u",
			ф : "f",
			х : "h",
			ц : "tz",
			ч : "ch",
			ш : "sh",
			щ : "shch",
			ъ : "yy",
			ы : "iy",
			ь : "ii",
			э : "ee",
			ю : "yu",
			я : "ya"
		};
		for(var i=0; i < text.length; i++)
		{
			let letter = text.substr(i, 1);
			text = text.replace(letter, r[letter]);
		}
		console.log(text);
		return text;
	}
}