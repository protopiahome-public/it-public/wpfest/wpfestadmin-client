import React, {Component, Fragment} from "react";
import buttons from "./buttons.json";
import {__} from "../../utilities/Voc";

import Settings from "../cabinet/Settings";
import Content from "../cabinet/Content";
import Statistics from "../cabinet/Statistics";
import Projects from "../cabinet/Projects";
import Criteries from "../cabinet/Criteries";
import Categories from "../cabinet/Categories";
import Ganres from "../cabinet/Ganres";
import Users from "../cabinet/Users";
import ShortExperts from "../cabinet/ShortExperts";
import ShortMembers from "../cabinet/ShortMembers";
import FullExperts from "../cabinet/FullExperts";
import FullMembers from "../cabinet/FullMembers";
import CreatedUsers from "../cabinet/CreatedUsers";

import {  
	Icon, 
	Intent, 
	InputGroup,
	Card, 
	Elevation, 
	Button, 
	ButtonGroup, 
	TextArea, 
	Tooltip,
	Position,
	PopoverInteractionKind, 
	Popover,
	MenuItem
} from "@blueprintjs/core";

export default class Panel extends Component
{
	panels()
	{
		return {
			"Settings" 	: Settings,
			"Content" 	: Content,
			"Statistics": Statistics,
			"Projects"	: Projects,
			"Users"		: Users,
			"Criteries"	: Criteries,
			"Categories": Categories,
			"Ganres"	: Ganres,
			"ShortMembers"	: ShortMembers,
			"ShortExperts"	: ShortExperts,
			"FullExperts"	: FullExperts,
			"FullMembers"	: FullMembers,
			"CreatedUsers"	: CreatedUsers,
		};
	}
	render()
	{
		//console.log( this.props.data, this.props.currentButton.component );
		const MyPanel = this.panels()[ this.props.currentButton.component ];
		return <div className="w-100">
			<div className="display-4 p-4 d-flex align-items-center">
				<Icon icon={ this.props.currentButton.icon } iconSize={25} />  
				<span className="ml-3 text-center">
					{ __(this.props.currentButton.title2 || this.props.currentButton.title) }
				</span>
			</div>
			
			<div className="px-4 py-1 mb-2">
				<div className="float-right">
					<a 
						className="small"
						href={this.props.url}
						target="_blank"
					>
						{__("Goto")}
					</a>
				</div>
				<div className="small mb-1">{this.props.url}</div>						
			</div>
			<MyPanel 
				data={this.props.data} 
				format={this.props.data.format}  /* data type */
				termin={this.props.data.termin}  /* post, term or user */
				blog_id={this.props.blog_id}
				theadColor={ this.props.currentButton.color || "#0E5A8A" }
			/>
		</div>
	}
}