import React, {Component, Fragment} from "react";
import buttons from "./buttons.json";
import {__} from "../../utilities/Voc";
import {  
	Icon, 
	Intent, 
	InputGroup,
	Card, 
	Elevation, 
	Button, 
	ButtonGroup, 
	TextArea, 
	Tooltip,
	Position,
	PopoverInteractionKind, 
	Popover,
	MenuItem
} from "@blueprintjs/core";

export default class ButtonList extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			
		};
	}
	render()
	{
		const btns = buttons.map((elem, i) =>
		{
			return <div 
				className={ this.props.active == i 
					? "bg-light text-dark fla uabtn pointer" 
					: "text-secondary uabtn pointer"
				}
				style={{ margin: "0 -15px 0 -15px", textAlign:"center" }}
				n={i}
				key={i}
				onClick={this.onClick}
			>
				<Icon icon={elem.icon}  iconSize={20} /> <div className="small mt-1">{__(elem.title)}</div>
			</div>
		});
		return <Fragment>
			{btns}
		</Fragment>
	}
	onClick = evt =>
	{
		const active = evt.currentTarget.getAttribute("n");
		this.props.switchPanel(active)
	}
}