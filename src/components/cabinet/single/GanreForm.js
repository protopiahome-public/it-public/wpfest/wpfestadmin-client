import React, {Component, Fragment} from "react";
import {__} from "../../../utilities/Voc";
import MediaChooser from "../../../utilities/MediaChooser";
import ColorPicker from "../../../utilities/ColorPicker";
import { 
	Icon, Tag, 
	Intent, Tooltip, 
	Card, FormGroup, 
	Button, ButtonGroup,
	Position, Popover, 
	InputGroup 
} from "@blueprintjs/core";
 
export default class GanreForm extends Component
{
	state = {
		post_title 	: this.props.post_title,
		color 		: this.props.color,
		icon 		: this.props.meta.icon,
		icon_id 	: this.props.meta.icon_id,
		
	}
	title = evt =>
	{
		const txt = evt.currentTarget.value;
		//this.props.onChange("post_title", txt, this.props.ID);
		this.setState({post_title : txt});
	}
	onColor = color =>
	{
		// console.log(color);
		//this.props.onChange("color", color, this.props.ID);
		this.setState({color : color.hex});
	}
	onIcon=(url, file, ID)=>
	{
		this.setState({ icon_id : -1, icon:url, icon_name: file.name });
	}
	onSave = () =>
	{
		this.props.onSave( this.state, this.props.ID );
	}
	onDelete =() =>
	{
		this.props.onDelete( this.props.ID );
	}
	render()
	{		
		//console.log(this.props);
		const del_btn = this.props.isNew ? null : <Popover
					position={Position.TOP_LEFT}
					content={
						<div className="square p-3">
							<p>
								{__("Are you realy want delete?")}
							</p>
							<ButtonGroup className="p-2 tex-right">
								<Button
									intent={Intent.DANGER}
									text={__("Yes")}
									onClick={this.onDelete}
								/>
							</ButtonGroup>
						</div>						
					}
				>
					<Button
						text={__("Delete")}
					/>
				</Popover>
				
		return <Fragment>
			<FormGroup
				label={__("Title")}
				helperText="insert title by any symbols"
				className="p-2"
			>
				<InputGroup 
					leftIcon="new-text-box" 
					fill={true}  
					value={this.state.post_title}
					onChange={ this.title}
				/>
			</FormGroup>
			<FormGroup
				label={__("Icon")}
				helperText="Upload image file no more 500kB and .svg, .bmp, .jpg, .png or .gif format"
				className="p-2"
			>
				<MediaChooser
					prefix="icon"
					id={12}
					url={this.state.icon}
					height={50}
					padding={50}
					bg={this.state.color}
					name={this.state.icon_name}
					onChange={this.onIcon}
				/>
			</FormGroup>
			<FormGroup
				label={__("Color")}
				helperText="Choose color"
				className="p-2"
			>
				<ColorPicker  
					color={this.state.color}
					onChoose={this.onColor}
				/>
			</FormGroup>
			<ButtonGroup className="p-2 tex-right">
				<Button
					text={__("Save")}
					onClick={this.onSave}
				/>
				{del_btn}
				<Button
					icon="undo"
					onClick={this.props.onClose}
				/>
			</ButtonGroup>
		</Fragment>
	}
}