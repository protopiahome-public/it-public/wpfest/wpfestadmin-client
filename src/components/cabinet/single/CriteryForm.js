import React, {Component, Fragment} from "react";
import {__} from "../../../utilities/Voc";
import Dropdown from "../../../utilities/Dropdown";
import GanreRange from "../../../utilities/GanreRange";

import { 
	Icon, Tag, 
	Intent, Tooltip, 
	Card, FormGroup, 
	Button, ButtonGroup,
	Position, Popover, 
	InputGroup 
} from "@blueprintjs/core";
 
export default class CriteryForm extends Component
{
	state = {
		post_title 		: this.props.post_title,
		color 			: this.props.color,
		fmru_category	: this.props.meta.fmru_category && this.props.meta.fmru_category.length 
			? this.props.meta.fmru_category.map(elem => elem.id) 
			: []
	}
	title = evt =>
	{
		const txt = evt.currentTarget.value;
		this.props.onChange("post_title", txt, this.props.ID);
		this.setState({post_title : txt});
	}
	onCategory = n =>
	{
		console.log(n);
		this.setState({fmru_category : n});
	}
	onGanre = n =>
	{
		this.setState({fmru_ganre : n});
	}
	onSave = () =>
	{
		this.props.onSave( this.state, this.props.ID );
	}
	onDelete =() =>
	{
		this.props.onDelete( this.props.ID );
	}
	render()
	{		
		//console.log(this.props);
		const del_btn = this.props.isNew ? null : <Popover
					position={Position.TOP_LEFT}
					content={
						<div className="square p-3">
							<p>
								{__("Are you realy want delete?")}
							</p>
							<ButtonGroup className="p-2 tex-right">
								<Button
									intent={Intent.DANGER}
									text={__("Yes")}
									onClick={this.onDelete}
								/>
							</ButtonGroup>
						</div>
						
					}
				>
					<Button
						text={__("Delete")}
					/>
				</Popover>
				
		return <Fragment>
			<FormGroup
				label={__("Title")}
				helperText="insert title by any symbols"
				className="p-2"
			>
				<InputGroup 
					leftIcon="new-text-box" 
					fill={true}  
					value={this.state.post_title}
					onChange={ this.title}
				/>
			</FormGroup>
			<FormGroup
				label={__("Parent Category")}
				helperText="Choose category"
				className="p-2"
			>
				<Dropdown
					options={this.props.meta.fmru_categories}
					current={this.state.fmru_category}
					multiple={true}
					className="btn-outline-secondary"
					onClick={this.onCategory}
				/>
			</FormGroup>
			<ButtonGroup className="p-2 tex-right">
				<Button
					text={__("Save")}
					onClick={this.onSave}
				/>
				{del_btn}
				<Button
					icon="undo"
					onClick={this.props.onClose}
				/>
			</ButtonGroup>
		</Fragment>
	}
}