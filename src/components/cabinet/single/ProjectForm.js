import React, {Component, Fragment} from "react";
import {__} from "../../../utilities/Voc";
import MyEditor from "../../../utilities/Editor";
import GanreRange from "../../../utilities/GanreRange";

import { 
	Icon, Tag, 
	Intent, Tooltip, 
	Card, FormGroup, 
	Button, ButtonGroup,
	Position, Popover, 
	InputGroup, NumericInput
} from "@blueprintjs/core";
 
export default class ProjectForm extends Component
{
	state = {
		post_title 		: this.props.post_title,
		post_content	: this.props.post_content,
		color 			: this.props.color,
		order 			: parseInt(this.props.meta.order),
		fmru_ganre		: this.props.meta.fmru_ganre && this.props.meta.fmru_ganre.length 
			? this.props.meta.fmru_ganre.map(elem => elem.id)
			: []
	}
	title = evt =>
	{
		const txt = evt.currentTarget.value;
		this.props.onChange("post_title", txt, this.props.ID);
		this.setState({post_title : txt});
	}
	onOrder = n =>
	{
		//console.log( n );
		this.setState({order : n});
	}
	onDescrEdit = text =>
	{
		//console.log( n );
		this.setState({post_content : text});
	}
	onCategory = n =>
	{
		this.setState({fmru_ganre : n});
	}
	onSave = () =>
	{
		//console.log( this.state );
		this.props.onSave( this.state, this.props.ID );
	}
	onDelete =() =>
	{
		this.props.onDelete( this.props.ID );
	}
	render()
	{		
		console.log(this.state);
		const del_btn = this.props.isNew ? null : <Popover
					position={Position.TOP_LEFT}
					content={
						<div className="square p-3">
							<p>
								{__("Are you realy want delete?")}
							</p>
							<ButtonGroup className="p-2 tex-right">
								<Button
									intent={Intent.DANGER}
									text={__("Yes")}
									onClick={this.onDelete}
								/>
							</ButtonGroup>
						</div>
						
					}
				>
					<Button
						text={__("Delete")}
					/>
				</Popover>
				
		return <Fragment>
			<FormGroup
				label={__("Title")}
				helperText="insert title by any symbols"
				className="p-2"
			>
				<InputGroup 
					leftIcon="new-text-box" 
					fill={true}  
					large={true}  
					value={this.state.post_title}
					onChange={ this.title}
				/>
			</FormGroup>
			<FormGroup
				label={__("Order")}
				helperText="Position this Prjoject's in list"
				className="p-2"
			>
				<NumericInput 
					leftIcon="sort-asc" 
					fill={false} 
					large={true}  
					value={this.state.order}
					onValueChange={ this.onOrder}
				/>
			</FormGroup>
			<FormGroup
				label={__("Order")}
				helperText="Position this Prjoject's in list"
				className="p-2"
			>
				<MyEditor onChange={this.onDescrEdit} text={this.state.post_content}/>
			</FormGroup>
			<FormGroup
				label={__("Parent Ganre")}
				helperText="Choose Ganre"
				className="p-2"
			>
				 <GanreRange
					fmru_ganres={this.props.meta.fmru_ganres}
					name={"ganres"}
					checked={this.state.fmru_ganre}
					onChange={this.onCategory}
				 />
				 
			</FormGroup>
			<ButtonGroup className="p-2 tex-right">
				<Button
					text={__("Save")}
					onClick={this.onSave}
				/>
				{del_btn}
				<Button
					icon="undo"
					onClick={this.props.onClose}
				/>
			</ButtonGroup>
		</Fragment>
	}
}