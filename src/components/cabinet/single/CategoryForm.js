import React, {Component, Fragment} from "react";
import {__} from "../../../utilities/Voc";
import ColorPicker from "../../../utilities/ColorPicker";
import { 
	Icon, Tag, 
	Intent, Tooltip, 
	Card, FormGroup, 
	Button, ButtonGroup,
	Position, Popover, 
	InputGroup 
} from "@blueprintjs/core";
 
export default class CategoryForm extends Component
{
	state = {
		post_title 	: this.props.post_title,
		color 		: this.props.color,
		
	}
	title = evt =>
	{
		const txt = evt.currentTarget.value;
		this.props.onChange("post_title", txt, this.props.ID);
		this.setState({post_title : txt});
	}
	color = color =>
	{
		this.props.onChange("color", color, this.props.ID);
		this.setState({color : color.hex});
	}
	onSave = () =>
	{
		this.props.onSave( this.state, this.props.ID );
	}
	onDelete =() =>
	{
		this.props.onDelete( this.props.ID );
	}
	render()
	{		
		console.log(this.props);
		const del_btn = this.props.isNew ? null : <Popover
					position={Position.TOP_LEFT}
					content={
						<div className="square p-3">
							<p>
								{__("Are you realy want delete?")}
							</p>
							<ButtonGroup className="p-2 tex-right">
								<Button
									intent={Intent.DANGER}
									text={__("Yes")}
									onClick={this.onDelete}
								/>
							</ButtonGroup>
						</div>						
					}
				>
					<Button
						text={__("Delete")}
					/>
				</Popover>
				
		return <Fragment>
			<FormGroup
				label={__("Title")}
				helperText="insert title by any symbols"
				className="p-2"
			>
				<InputGroup 
					leftIcon="new-text-box" 
					fill={true}  
					value={this.state.post_title}
					onChange={ this.title}
				/>
			</FormGroup>
			<FormGroup
				label={__("Color")}
				helperText="Choose color"
				className="p-2"
			>
				<ColorPicker  
					color={this.state.color}
					onChoose={this.color}
				/>
			</FormGroup>
			<ButtonGroup className="p-2 tex-right">
				<Button
					text={__("Save")}
					onClick={this.onSave}
				/>
				{del_btn}
				<Button
					icon="undo"
					onClick={this.props.onClose}
				/>
			</ButtonGroup>
		</Fragment>
	}
}