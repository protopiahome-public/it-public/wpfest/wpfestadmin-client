import React, {Component, Fragment} from "react";
import {__} from "../../../utilities/Voc";

import { 
	Icon, Tag, 
	Intent, Tooltip, 
	Card, FormGroup, 
	Button, ButtonGroup,
	Position, Popover, 
	InputGroup, NumericInput
} from "@blueprintjs/core";
 
export default class UserForm extends Component
{
	state = {
		display_name	: this.props.meta.display_name,
		user_email		: this.props.meta.user_email,
		roles			: this.props.meta.roles && this.props.meta.roles.length 
			? this.props.meta.roles.map(elem => elem.id)
			: [],
		role			: this.props.meta.role
	}
	display_name = evt =>
	{
		const txt = evt.currentTarget.value;
		this.props.onChange("display_name", txt, this.props.ID);
		this.setState({display_name : txt});
	}
	onRole = (evt) =>
	{
		const checked = evt.currentTarget.checked;
		const rol = evt.currentTarget.value;
		let role = this.state.role.slice(0);
		
		if(checked)
		{
			role.push(rol);
		}
		else
		{
			role = role.filter(e => e != rol);
		}
		console.log(role);
		this.setState({role});
	}
	onSave = () =>
	{
		//console.log( this.state );
		this.props.onSave( this.state, this.props.ID );
	}
	onDelete =() =>
	{
		this.props.onDelete( this.props.ID );
	}
	render()
	{		
		console.log(this.props);
		const del_btn = this.props.isNew ? null : <Popover
					position={Position.TOP_LEFT}
					content={
						<div className="square p-3">
							<p>
								{__("Are you realy want delete?")}
							</p>
							<ButtonGroup className="p-2 tex-right">
								<Button
									intent={Intent.DANGER}
									text={__("Yes")}
									onClick={this.onDelete}
								/>
							</ButtonGroup>
						</div>
						
					}
				>
					<Button
						text={__("Delete")}
					/>
				</Popover>
		const roles = this.props.meta.roles.map(elem =>
		{
			return <div key={elem.id} className="mb-1">
				<input					
					type="checkbox"
					className="_checkbox"
					value={elem.id}
					id={elem.id}
					onChange={this.onRole}
					checked={ this.state.role.filter(e=> elem.id==e).length > 0 }
				/>
				<label htmlFor={elem.id}>{elem.post_title}</label>
			</div>
		})		
		return <Fragment>
			<FormGroup
				label={__("Title")}
				helperText="insert title by any symbols"
				className="p-2"
			>
				<InputGroup 
					leftIcon="person" 
					fill={true}  
					large={true}  
					value={this.state.display_name}
					onChange={ this.display_name}
				/>
			</FormGroup>
			<FormGroup
				label={__("Roles")}
				helperText="Choose one or mpre Roles"
				className="p-2"
			>
				{roles}	
				 
			</FormGroup>
			<ButtonGroup className="p-2 tex-right">
				<Button
					text={__("Save")}
					onClick={this.onSave}
				/>
				{del_btn}
				<Button
					icon="undo"
					onClick={this.props.onClose}
				/>
			</ButtonGroup>
		</Fragment>
	}
}