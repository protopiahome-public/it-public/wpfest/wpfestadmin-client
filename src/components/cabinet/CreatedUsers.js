import React, {Component, Fragment} from "react";
import _fetch from "../../api";
import {__} from "../../utilities/Voc";

export default class CreatedUsers extends Component
{
	state ={post:[]};
	componentWillMount()
	{
		_fetch("lead_fest_created_users", {id:this.props.blog_id})
			.then(data =>
			{		
				//console.log(data);
				this.setState({post:data.lead.post})
			});
	}
	render()
	{
		const tbl = this.state.post.map(elem => <tr key={elem.id} >
			<td className="font-weight-bold">{elem.display_name}</td>
			<td>{elem.pass}</td>
			<td>{ elem.role.map(e => <span className=" member_span">{e}</span> )}</td>
		</tr>);
		
		return <table className="table table-striped mb-5" >
			<thead className="thead-dark">
				<tr>
					<td>{__("Name")}</td>
					<td>{__("Password")}</td>
					<td>{__("Roles")}</td>
				</tr>
			</thead>
			<tbody>
				{tbl}
			</tbody>
		</table>;
	}
}