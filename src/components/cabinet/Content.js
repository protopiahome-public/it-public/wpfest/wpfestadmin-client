import React, {Component, Fragment} from "react";
import {__} from "../../utilities/Voc";
import MyEditor from "../../utilities/Editor";
import _fetch from "../../api";
import { Icon, Tag, Intent, Tooltip, Card, FormGroup, InputGroup, NumericInput, TextArea, Button } from "@blueprintjs/core";

export default class Content extends Component
{
	state ={
		name		: '',
		url			: '',
		descr		: '',
		description	: '',
		domain_content : '',
		blog_id		: -1,
		author		: {},
		prototyper	: {},
		isLoad		: false
	};
	componentWillMount()
	{
		_fetch("lead_fest_content", {id:this.props.blog_id})
			.then(data =>
			{		
				console.log(data);
				this.setState({
					name		: data.lead.name,
					url			: data.lead.url,
					domain_type	: data.lead.domain_type,
					domain_type_id		: data.lead.domain_type_id,
					domain_description	: data.lead.domain_description,
					descr		: data.lead.descr,
					description	: data.lead.description,
					domain_content		: data.lead.domain_content,
					blog_id		: data.lead.id,
					author		: data.lead.author || {},
					prototyper	: data.lead.prototyper || {},
					isLoad		: true
				});				
			});
	}
	onName = evt =>
	{
		this.setState({name : evt.currentTarget.value });
	}
	onDescrEdit = text =>
	{
		console.log(text);
		this.setState({domain_content : text });
	}
	onDescr = evt =>
	{
		this.setState({descr : evt.currentTarget.value });
	}
	onDescription = evt =>
	{
		this.setState({description : evt.currentTarget.value });
	}
	onUpdate = () =>
	{
		_fetch("lead_fest_content_update", {id:this.props.blog_id, data:this.state})
			.then(data =>
			{		
				console.log(data);	
			});
	}
	render()
	{
		
		return !this.state.isLoad ? null : <div className='row'>
			<div className='col-12 mb-5'>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Parameters")}</div>
					<div className=" mt-3 row">
						<div className="col-md-2 small">{__("Model:")} </div>
						<div className="col-md-10 font-weight-bold text-danger">
							{this.state.domain_type}
						</div>
					</div>
					<div className=" mb-3 row">
						<div className="col-md-2 small">{__("Description:")} </div>
						<div className="col-md-10 text-danger small">
							{this.state.domain_description}
						</div>
					</div>
					<FormGroup
						label={__("Title")}
						helperText="insert title by any symbols"
						className="p-2"
					>
						<InputGroup  
							fill={true}  
							large={true}  
							value={this.state.name}
							onChange={ this.onName}
						/>
					</FormGroup>
					
					<FormGroup
						label={__("Undertitle")}
						helperText="insert title by any symbols"
						className="p-2"
					>
						<InputGroup 
							className="text-secondary"								
							fill={true}  
							large={false}  
							value={this.state.description}
							onChange={ this.onDescription}
						/>
					</FormGroup>
					<FormGroup
						label={__("Content of description page")}
						helperText="Set description"
						className="p-2"
					>
						<MyEditor 
							text={this.state.domain_content}
							onChange={this.onDescrEdit}
						/>
					</FormGroup>
				</Card>
				<div className="my-3">
					<Button
						text={__("Update")}
						onClick={this.onUpdate}
					/>
				</div>
			</div>
		</div>;
	}
}