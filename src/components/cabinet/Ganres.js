import React, {Fragment,Component} from "react";
import {__} from "../../utilities/Voc";
import Categories from "./Categories";
import GanreForm from "./single/GanreForm";
import { Icon, Tag, Intent, Tooltip } from "@blueprintjs/core";

export default class Ganres extends Categories
{
	constructor(props)
	{
		super(props);
		this.init({
			form_component	: GanreForm,		
			meta			: {color: "#FFF", icon:"", icon_id:-1}
		})
	}
	rows()
	{
		let rw = super.rows();
		rw.splice(1,0,["icon", <Tooltip intent={Intent.DANGER} content={__("Icon")}><Icon icon="grid-view" /></Tooltip>,100]);
		return rw;
	}
	
	onRow(col, elem)
	{
		let txt = super.onRow(col, elem);
		switch(col)
		{
			case "icon":
				txt = <div className="mycro_icon" style={{backgroundImage:"url("+elem.icon+")"}}/>
				break;
		}
		return txt;
	}
}