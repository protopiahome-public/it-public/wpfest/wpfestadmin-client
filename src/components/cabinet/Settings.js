import React, {Fragment,Component} from "react";
import {__} from "../../utilities/Voc";
import _fetch from "../../api";
import MediaChooser from "../../utilities/MediaChooser";
import {  
	Icon, 
	Intent, 
	InputGroup,
	Card, 
	Elevation, 
	Button, 
	ButtonGroup, 
	TextArea, 
	Tooltip,
	Position,
	PopoverInteractionKind, 
	Popover,
	Slider
} from "@blueprintjs/core";

export default class Settings extends Component
{
	constructor(props)
	{
		super(props);
		this.state = 
		{
			status : this.props.data.status,
			max_raiting : 0,
			onIsEnabledRules : 0,
			fmru_evlbl_roles :[],
			all_roles :[],
			is_experts_criteries : 0,
			is_register : 0,
			is_ganres : 0,
			is_diaries : 0,
			member_card : "box",
			card_count : 10,
			icon:"",
			icon_id:"",
			default_member_thrumb:"",
			default_member_thrumb_id:"",
		}
	}
	componentWillMount() 
	{
		//console.log(this.props.blog_id);
		_fetch("site", {id:this.props.blog_id})
			.then(data =>
			{
				//console.log( data );
				this.setState({
					status : data.site.options.status,
					icon : data.site.icon,
					icon_id : data.site.icon_id,
					default_member_thrumb : data.site.default_member_thrumb,
					default_member_thrumb_id : data.site.default_member_thrumb_id,
					all_roles : data.site.roles,
					member_card : data.site.options.member_card,
					fmru_evlbl_roles : data.site.options.fmru_evlbl_roles,
					max_raiting : data.site.options.max_raiting ? parseInt(data.site.options.max_raiting) : 3,
					is_experts_criteries : data.site.options.is_experts_criteries == "1",
					is_register : data.site.options.is_register == "1",
					is_ganres : data.site.options.is_ganres == "1",
					is_diaries : data.site.options.is_diaries == "1",
					enabled_rules : data.site.options.enabled_rules == "1",
					card_count : data.site.options.member_per_page ? parseInt(data.site.options.member_per_page): 100
					
				});
			});
	}
	render()
	{
		//console.log( this.state );
		const frm = ["Before starting Festival", "Project preparation phase", "Project submission phase", "Festival was closed"]
			.map((elem, i) => <Fragment key={i}>
				<div className='mb-1'>					
						<input 
							type='radio' 
							name='status'
							className='radio_full' 
							id={'status' + i}
							value={elem}
							checked={this.state.status == i}
							onChange={this.onPhase}
							n={i}
						/>
						<label htmlFor={'status' + i} data-hint={__(elem)}>
							<div className={ this.state.status == i ? "cat_color_label ggreen" : "cat_color_label"} />
						</label>
					
				</div>
			</Fragment>);
		const member_card = ['card','stroke','box'].map((elem, i) => <Fragment key={i}>
				<div className='mb-1'>					
						<input 
							type='radio' 
							name='member_card'
							className='radio_full' 
							id={'member_card' + i}
							value={elem}
							checked={this.state.member_card == elem}
							onChange={this.onMemberCard}
							n={i}
						/>
						<label htmlFor={'member_card' + i} data-hint={__(elem)}>
							<div className={ this.state.member_card == elem ? "cat_color_label ggreen" : "cat_color_label"} />
						</label>
					
				</div>
			</Fragment>);
		const roles = this.state.all_roles.map(elem =>
		{
			return <div key={elem} className="mb-1">
				<input					
					type="checkbox"
					className="_checkbox"
					value={elem}
					id={elem}
					onChange={this.onRole}
					checked={ this.state.fmru_evlbl_roles.filter(e=> elem==e).length > 0 }
				/>
				<label htmlFor={elem}>{__(elem)}</label>
			</div>
		})	
		return <div className='row'>
			<div className='col-12 mb-5'>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Status")}</div>
					{frm}
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Icons")}</div>
					<div className="row">
						<div className="col-6">
							<MediaChooser
								url={this.state.icon}
								prefix={"icon"}
								onChange={this.onIcon}
								id={this.state.icon_id}
							/>
							<div className="small mt-1">{__("Logotype")}</div>
						</div>
						<div className="col-6">
							<MediaChooser
								url={this.state.default_member_thrumb}
								prefix={"deficon"}
								onChange={this.onDefIcon}
								id={this.state.default_member_thrumb_id}
							/>
							<div className="small mt-1">{__("Default Project's icon")}</div>
						</div>
					</div>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Max raiting setting")}</div>
					 <Slider
						min={0}
						max={10}
						stepSize={1}
						labelStepSize={10}
						onChange={this.onMaxRating}
						value={this.state.max_raiting}
						vertical={false}
					/>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Count of Member's Cards per screen")}</div>
					 <Slider
						min={0}
						max={200}
						stepSize={8}
						labelStepSize={10}
						onChange={this.onCardCount}
						value={this.state.card_count}
						vertical={false}
					/>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Experts's criteries enabled")}</div>
					 <input
						type="checkbox"
						className="_checkbox"
						checked={this.state.is_experts_criteries}
						onChange={this.onIsExpertCriteries}
						id="is_experts_criteries"
					/>
					<label htmlFor="is_experts_criteries"> </label>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Is ganres exists")}</div>
					 <input
						type="checkbox"
						className="_checkbox"
						checked={this.state.is_ganres}
						onChange={this.onIsGanres}
						id="is_ganres"
					/>
					<label htmlFor="is_register"></label>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Visitors can create accounts")}</div>
					 <input
						type="checkbox"
						className="_checkbox"
						checked={this.state.is_register}
						onChange={this.onIsRegister}
						id="is_register"
					/>
					<label htmlFor="is_register"></label>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Project authors keep a diary")}</div>
					 <input
						type="checkbox"
						className="_checkbox"
						checked={this.state.is_diaries}
						onChange={this.onIsDiareis}
						id="is_diaries"
					/>
					<label htmlFor="is_diaries"></label>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-1">{__("Page of Festival rules")}</div>
					 <input
						type="checkbox"
						className="_checkbox"
						checked={this.state.enabled_rules}
						onChange={this.onIsEnabledRules}
						id="enabled_rules"
					/>
					<label htmlFor="enabled_rules"></label>
					<small className="text-secondary" style={{ position:"absolute", marginTop:10 }}>
						{__("Content of this page put to «About» link.")}
					</small>
				</Card>
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-2">{__("Default Member's card")}</div>
					{member_card}
				</Card>
				
				<Card fill={"true"} interactive={true} className="p-4 mb-2">
					<div className="lead mb-2">{__("Evalble special roles")}</div>
					{roles}
				</Card>	
			</div>
		</div>;
	}
	onUpdate = (param, value) =>
	{
		console.log(param, value);
		_fetch("updte_site", {param : param, value : value, blog_id:this.props.blog_id})
			.then(data =>
			{
				console.log( data );
				let state = Object.assign({}, this.state);
				state[param] = value;				
				this.setState(state);
			});
	}
	onRole = evt =>
	{
		const checked = evt.currentTarget.checked;
		const rol = evt.currentTarget.value;
		let fmru_evlbl_roles = this.state.fmru_evlbl_roles.slice(0);		
		if(checked)
		{
			fmru_evlbl_roles.push(rol);
		}
		else
		{
			fmru_evlbl_roles = fmru_evlbl_roles.filter(e => e != rol);
		}
		console.log(fmru_evlbl_roles);
		this.onUpdate("fmru_evlbl_roles", fmru_evlbl_roles);
		//this.setState({ status : n });
	}
	onPhase = evt =>
	{
		const n = parseInt(evt.currentTarget.getAttribute("n"));
		this.onUpdate("status", n);
		//this.setState({ status : n });
	}
	onMemberCard = evt =>
	{
		const value = evt.currentTarget.value;
		this.onUpdate("member_card", value);
		this.setState({ member_card : value });
	}
	onMaxRating = n =>
	{
		this.onUpdate("max_raiting", n);
		this.setState({max_raiting:n});
	}
	onCardCount = n =>
	{
		this.onUpdate("member_per_page", n);
		this.setState({card_count:n});
	}
	onIcon = ( url, file, ID ) =>
	{
		//console.log( url, file, ID );
		this.onUpdate( "logotype_0", { url:url, name:file.name } );
		this.setState({icon:url});
	}
	onDefIcon = ( url, file, ID ) =>
	{
		this.onUpdate( "default_member_thrumb", { url:url, name:file.name } );
		this.setState({default_member_thrumb:url});
	}
	onIsExpertCriteries = evt =>
	{
		const val = evt.currentTarget.checked ? 1 : 0;
		this.onUpdate("is_experts_criteries", val);
		this.setState({is_experts_criteries : val});
	}
	onIsEnabledRules = evt =>
	{
		const val = evt.currentTarget.checked ? 1 : 0;
		this.onUpdate("enabled_rules", val);
		this.setState({enabled_rules : val});
	}
	onIsRegister = evt =>
	{
		const val = evt.currentTarget.checked ? 1 : 0;
		this.onUpdate("is_register", val);
		this.setState({is_register : val});
	}
	onIsGanres = evt =>
	{
		const val = evt.currentTarget.checked ? 1 : 0;
		this.onUpdate("is_ganres", val);
		this.setState({is_ganres : val});
	}
	onIsDiareis = evt =>
	{
		console.log("onIsDiareis");
		const val = evt.currentTarget.checked ? 1 : 0;
		this.onUpdate("is_diaries", val);
		this.setState({is_diaries : val});
	}
}