
import React, {Fragment,Component} from "react";
import {__} from "../../utilities/Voc";
import _fetch from "../../api";
import { Icon, Tag, Intent } from "@blueprintjs/core";
import Categories from "./Categories";
import CriteryForm from "./single/CriteryForm";

export default class Criteries extends Categories
{	
	componentWillMount()
	{
		_fetch("lead_fest_cats", {id:this.props.blog_id} )
			.then(data =>
			{		
				//console.log(data);
				this.init({
					form_component	: CriteryForm,
					theadColor		: "#416348",	
					meta			: { fmru_category: {}, fmru_categories: data.lead.post }
				})
			});
	}
	rows()
	{
		let rw = super.rows().filter(elem => elem[0] != 'color');
		//console.log(rw);
		return rw;
	}
	getPostTitle = elem =>
	{
		const tags = elem.fmru_category.map(e => <Tag
					style={{backgroundColor : e.color}}
					interactive={true}
					key={e.id}
				>
					{e.post_title}
				</Tag>);
		return <Fragment>
			<div>{elem.post_title}</div>
			<div>{tags}</div>
		</Fragment>
	}
	/*
	render()
	{
		console.log( this.props );
		const projects = this.props.data.post.map(elem => {
			const tags = elem.fmru_category.map(e => <Tag
					style={{backgroundColor : e.color}}
					interactive={true}
					key={e.id}
				>
					{e.post_title}
				</Tag>);
			return <tr key={elem.id}>
				<td className="lead">{elem.order}</td>
				<td>
					<div>{elem.post_title}</div>
					<div>{tags}</div>
				</td>
				<td>
					<div className="btn btn-link btn-sm ">
						<Icon icon="edit" />
					</div>
				</td>
			</tr>
		});
		return <table className="table">
			<thead style={{backgroundColor:"#008075", color:"#EEE"}}>
				<tr>
					<th><Icon icon="sort-asc" /></th>
					<th>{__("Title")}</th>
					<th>
						<div className="btn btn-link text-light btn-sm">
							<Icon icon="plus" />
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				{projects}
			</tbody>
		</table>;
	}
	*/
}