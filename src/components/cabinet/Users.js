import React, {Fragment,Component} from "react";
import {__} from "../../utilities/Voc";
import _fetch from "../../api";
import { Icon, Tag, Intent, Tooltip, Card } from "@blueprintjs/core";
import Categories from "./Categories";
import UserForm from "./single/UserForm";
import UserBatchForm from "./single/UserBatchForm";

export default class Users extends Categories
{
	
	componentWillMount()
	{
		_fetch( "lead_fest_roles" , { id:this.props.blog_id } )
			.then( data =>
			{		
				console.log(data);
				data.lead.post.unshift("administrator");
				this.init({
					form_component	: UserForm,
					theadColor		: "#ab2da1",
					meta			: { 
						role 		: [], 
						roles 		: data.lead.post.map( e=> {return {post_title:__(e), id:e} } ),
						display_name: "",
						user_email 	: ""
					}
				})
			});
	}
	rows()
	{
		let rw = super.rows().filter(elem => elem[0] != 'color' && elem[0] != 'title');
		rw.unshift([
			'user',
			<div className="d-flex">
				<span className="mx-1">{__("User")}</span> <span className="ml-auto">{__("do Boo")}</span>
			</div>
		]);
		return rw;
	}
	onRow(col, elem)
	{
		let txt = super.onRow(col, elem);
		switch(col)
		{
			case "user":
				const tags = elem.role.map((e, i) => <Tag
							intent={Intent.NONE}
							interactive={true}
							className="mr-1"
							key={i}
						>
							{ __(e) }
						</Tag>);
				txt = <Fragment><div className="lead mb-1">{elem.display_name}</div><div>{tags}</div></Fragment>
				break;
		}
		return txt;
	}
	
	row_edit()
	{
		return <span className="ml-2">
			<Tooltip intent={Intent.DANGER} content={__(!this.state.is_batch_creation ? "batch creation" : "close")}>
				<Icon icon={!this.state.is_batch_creation ? "list" : "undo" } onClick={this.onBitchCreation}/>
			</Tooltip>
		</span>
	}
	onBitchCreation = () =>
	{
		this.setState({is_batch_creation:!this.state.is_batch_creation});
	}
	
	addThead()
	{
		return this.state.is_batch_creation ?
		<tr>
			<td colSpan={13}>
				<UserBatchForm 
					roles = {this.meta.roles}
					goClose = {this.onBitchCreation}
					onCreate = {this.onCreate}
					blog_id = {this.props.blog_id}
				/>
			</td>
		</tr> : null;
	}
	onCreate = data =>
	{
		this.setState({post:data});
	}
	onBitchCreation = () =>
	{
		this.setState({is_batch_creation:!this.state.is_batch_creation, isNew:false});
	}
	/*
	render()
	{
		console.log( this.props );
		const projects = this.props.data.post.map((elem, n) => {
			const tags = elem.role.map((e, i) => <Tag
					intent={Intent.NONE}
					interactive={true}
					className="mr-1"
					key={i}
				>
					{ __(e) }
				</Tag>);
			return <tr key={n}>
				<td>
					<div>{elem.display_name}</div>
					<div>{tags}</div>
				</td>
				<td>
					<div className="btn btn-link btn-sm ">
						<Icon icon="edit" />
					</div>
				</td>
			</tr>
		});
		return <table className="table">
			<thead style={{backgroundColor:"#728C23", color:"#EEE"}}>
				<tr>
					<th>{__("User")}</th>
					<th>
						<div className="btn btn-link text-light btn-sm">
							<Icon icon="plus" />
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				{projects}
			</tbody>
		</table>;
	}
	*/
}