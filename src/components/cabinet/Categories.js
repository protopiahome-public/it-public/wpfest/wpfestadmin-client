
import React, {Fragment,Component} from "react";
import {__} from "../../utilities/Voc";
import _fetch from "../../api";
import ColorPicker from "../../utilities/ColorPicker";
import CategoryForm from "./single/CategoryForm";
import {AppToaster} from "../../utilities/blueUtils";
import { 
	Icon, Tag, 
	Intent, Tooltip, 
	Card, FormGroup, 
	Button, ButtonGroup,
	Position, Popover, 
	InputGroup 
 } from "@blueprintjs/core";

export default class Categories extends Component
{
	constructor(props)
	{
		super(props);
		//console.log(props);
		this.state = { 
			current:-1, 
			isNew:false, 
			post:this.props.data.post, 
			theadColor: this.props.theadColor || "#2d74ab" 
		};
		this.init({
			form_component	: CategoryForm,		
			meta			: {color: "#FFF"}
		})
	}
	
	render()
	{
		const Form_component = this.form_component;			
		const thead = this.state.isNew ? <tr>
				<td colSpan={12} style={{ backgroundColor : this.state.theadColor }}>
					<Card fill={"true"} className="p-4 text-dark w-100" interactive={true} >
						<Form_component
							ID={-1}
							post_title={""}
							post_content={""}
							meta={ this.meta }
							color={"#FFFFFF"}
							onChange={this.onChange}
							onSave={this.onSave}
							onDelete={this.onDelete}
							onClose={this.onClose}
							isNew={true}
						/>
					</Card>
				</td>
			</tr> : null;
		const projects = this.state.post.map(elem => 
		{		
			// console.log( elem );
			const style = { backgroundColor : elem.id==this.state.current ? "#8A9BA833": "transparent" };	
			let metas = {};
			for(var i in this.meta)
			{
				//console.log(i, this.meta[i]);
				metas[i] = elem[i] ? elem[i] : this.meta[i];
			}			
			const editForm = elem.id==this.state.current ? <tr>
				<td colSpan={12} style={style}>
					<Card fill={"true"} className="p-4" interactive={true} >
						<Form_component 
							ID={elem.id}
							post_title={elem.post_title}
							post_content={elem.post_content}
							meta={metas}
							color={elem['color']}
							onChange={this.onChange}
							onSave={this.onSave}
							onDelete={this.onDelete}
							onClose={this.onClose}
						/>
					</Card>
				</td>
			</tr> :
			<tr>
			{
				this.rows().map( e => <td key={e[0]}>{this.onDefRow(e[0], elem)}</td> )
			}
			</tr>;
			return <Fragment key={elem.id}>				
				{editForm}				
			</Fragment>
		});
		return <table className="table mb-5">
			<thead style={{backgroundColor : this.state.theadColor , color : "#EEE"}}>
				<tr>
				{
					this.rows().map( elem => <th col={ elem[0] } key={ elem[0] } width={elem[2]}>{ elem[1] }</th> )
				}
				</tr>
				{thead}
				{this.addThead()}
			</thead>
			<tbody>
				{projects}
			</tbody>
		</table>;
	}
	onEditForm = evt =>
	{
		const tid = parseInt(evt.currentTarget.getAttribute("tid"));
		this.setState({ current:tid, isNew:false })
	}
	onNew = () =>
	{
		this.setState({ current : -1, isNew : !this.state.isNew })
	}
	onClose=()=>
	{
		this.setState({ current : -1, isNew : false })
	}
	onChange=(field, value, id) =>
	{
		
	}
	onSave = (state, id) =>
	{
		//console.log("save", state, id );
		_fetch( id > 0 ? "update_object" : "insert_object", { 
			format 	: this.props.format, 
			termin 	: this.props.termin, 
			data	: state,
			id		: id,
			blog_id	: this.props.blog_id
		})
			.then(data =>
			{
				//console.log(data);
				if(data.msg)
					AppToaster.show({  
						intent: Intent.SUCCESS,
						icon: "tick", 
						timeout:10000,
						className: "px-4 py-4",
						message: data.msg			
					});
				if(data.post)	
					this.setState({ isNew:false, post:data.post })
			})
	}
	onDelete = id =>
	{
		_fetch("delete_object", { 
			format 	: this.props.format, 
			termin 	: this.props.termin, 
			id		: id,
			blog_id	: this.props.blog_id
		})
			.then(data =>
			{
				console.log(data);
				if(data.msg)
					AppToaster.show({  
						intent: Intent.SUCCESS,
						icon: "tick", 
						timeout:10000,
						className: "px-4 py-4",
						message: data.msg			
					});
				this.setState({ current:-1, isNew:false, post:this.state.post.filter(elem => elem.id != id) })
			})
	}
	
	/*
	//	override methods
	*/
	
	
	init(data)
	{
		this.form_component 	= data.form_component;
		this.meta		 		= data.meta;
		this.theadColor			= data.theadColor ||"#2d74ab";
		//this.setState({ theadColor: this.theadColor	});
	}
	
	rows()
	{
		return [
			['color',	<Tooltip intent={Intent.DANGER} content={__("Color")}><Icon icon="tint" /></Tooltip>, 60], 
			['title',	__("Title")],
			['edit',	
				[
					!this.state.isNew ? 
						<Tooltip intent={Intent.DANGER} content={__("Add new")} key={1}>
							<div className="btn btn-link text-light btn-sm" onClick={this.onNew}> 
								<Icon icon="plus" /> 
							</div>
						</Tooltip>
						:
						<Tooltip intent={Intent.DANGER} content={__("Cancel")} key={1}>
							<div className="btn btn-link text-light btn-sm" onClick={this.onNew}> 
								<Icon icon="undo" /> 
							</div>
						</Tooltip>,
					<span key={21}>{this.row_edit()}</span>
				], 120
			]
		];
	}
	onDefRow(col, elem)
	{
		let txt;
		switch(col)
		{
			case "title":
				txt = this.getPostTitle(elem);
				break;
			case "edit":
				txt = <div className="btn btn-link btn-sm " onClick={this.onEditForm} tid={elem.id} >
						<Icon icon="edit" />
					</div>
				break;
			default:
				txt = this.onRow(col, elem);
		}
		return txt;
	}
	onRow(col, elem)
	{
		let txt;
		switch(col)
		{
			case "color":
				txt = <div 
						className="color-display"
						style={{backgroundColor: elem.color }}
					/>
				break;
		}
		return txt;
	}
	getPostTitle(elem)
	{
		return <div>{elem.post_title}</div>;
	}
	row_edit()
	{
		return <span> </span>
	}
	addThead()
	{
		return null;
	}
}