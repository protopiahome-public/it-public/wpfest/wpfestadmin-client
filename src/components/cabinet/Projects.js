import React, {Fragment,Component} from "react";
import {__} from "../../utilities/Voc";
import _fetch from "../../api";
import { Icon, Tag, Intent, Tooltip, Card } from "@blueprintjs/core";
import Categories from "./Categories";
import ProjectForm from "./single/ProjectForm";
import ProjectBatchForm from "./single/ProjectBatchForm";

export default class Projects extends Categories
{
	componentWillMount()
	{
		_fetch("lead_fest_ganres", {id:this.props.blog_id})
			.then(data =>
			{		
				//console.log(this.state);
				this.init({
					form_component	: ProjectForm,
					theadColor		: "#ab2da1",
					meta			: { fmru_ganre : [], fmru_ganres : data.lead.post, order : parseInt(this.state.post[this.state.post.length - 1].order) + 1 }
				})
			});
	}
	row_edit()
	{
		return <span className="ml-2">
			<Tooltip intent={Intent.DANGER} content={__(!this.state.is_batch_creation ? "batch creation" : "close")}>
				<Icon icon={!this.state.is_batch_creation ? "layout-grid" : "undo" } onClick={this.onBitchCreation}/>
			</Tooltip>
		</span>
	}
	rows()
	{
		let rw = super.rows().filter(elem => elem[0] != 'color');
		rw.unshift(['order', <Tooltip intent={Intent.DANGER} content={__("Order")}><Icon icon="sort-asc" /></Tooltip>]);
		//console.log(rw);
		return rw;
	}
	onRow(col, elem)
	{
		let txt = super.onRow(col, elem);
		switch(col)
		{
			case "order":
				txt = <div className="lead">{elem.order}</div>
				break;
		}
		return txt;
	}
	getPostTitle = elem =>
	{
		const tags = elem.fmru_ganre.map(e => <Tag
					style={{backgroundColor : e.color, marginRight:2}}
					interactive={true}
					key={e.id}
				>
					{e.post_title}
				</Tag>);
		return <Fragment>
			<div>{elem.post_title}</div>
			<div>{tags}</div>
		</Fragment>
	}
	addThead()
	{
		return this.state.is_batch_creation ?
		<tr>
			<td colSpan={3}>
				<ProjectBatchForm 
					fmru_ganres = {this.meta.fmru_ganres}
					goClose = {this.onBitchCreation}
					onCreate = {this.onCreate}
					blog_id = {this.props.blog_id}
				/>
			</td>
		</tr> : null;
	}
	onCreate = data =>
	{
		this.setState({post:data});
	}
	onBitchCreation = () =>
	{
		this.setState({is_batch_creation:!this.state.is_batch_creation, isNew:false});
	}
	/*
	render()
	{
		console.log( this.props );
		const projects = this.props.data.projects.map(elem => {
			const tags = elem.ganres.map(e => <Tag
					style={{backgroundColor : e.color}}
					interactive={true}
					key={e.id}
				>
					{e.post_title}
				</Tag>);
			return <tr key={elem.id}>
				<td className="lead">{elem.order}</td>
				<td>
					<div>{elem.post_title}</div>
					<div>{tags}</div>
				</td>
				<td>
					<div className="btn btn-link btn-sm ">
						<Icon icon="edit" />
					</div>
				</td>
			</tr>
		});
		return <table className="table">
			<thead style={{backgroundColor:"#EB532D", color:"#EEE"}}>
				<tr>
					<th><Icon icon="sort-asc" /></th>
					<th>{__("Title")}</th>
					<th>
						<div className="btn btn-link text-light btn-sm">
							<Icon icon="plus" />
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				{projects}
			</tbody>
		</table>;
	}
	*/
}