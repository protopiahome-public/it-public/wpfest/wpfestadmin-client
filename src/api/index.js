import { MD5, base64_encode, base64_decode, get_url, getCookie } from "../utilities/utils";

export default function _fetch( code, args, log, psw, data )
{
	code = !code ? "page" : code;
	var headers = {
		'Accept': 'application/json',
		'Content-Type': 'application/json'
	};
	/*
	if(window.auth)
	{
		headers.Authorization = 'Basic ' + window.auth; 
	}
	else if(log && psw)
	{
		const xxx = base64_encode( log+':'+psw );
		headers.Authorization = 'Basic ' + xxx; 
	}
	*/
	if(typeof args != 'object')
		args = {};
	//headers.token = getCookie("token");
	if(window.atoken)
		headers.token = window.atoken;
	return fetch( get_url() + '/wp-json/wpfa/' + code,
	{
		method: 'POST',
		credentials: 'include',
		headers: headers,
		body: JSON.stringify({
			code: code,
			args: args
		})
	}) 
		.then( r => 
		{
			//console.log(r);
			const res = r.json();
			//console.log(res);
			return res;
		});
		
}