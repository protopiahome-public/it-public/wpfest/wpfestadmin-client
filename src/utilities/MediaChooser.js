import React, {Component} from 'react';
import {__} from "./Voc";
import $ from "jquery";
import in_array from "in_array";

export default class MediaChooser extends Component
{	
	constructor(props)
	{
		super(props);
		this.state = {
			id: props.id,
			name: props.name,
			prefix: props.prefix,
			url:props.url
		};
	}
	componentWillReceiveProps (nextProps) 
	{
		this.setState({
			id: nextProps.id,
			name: nextProps.name,
			prefix: nextProps.prefix,
			url:nextProps.url
		});
	}
	componentDidUpdate()
	{
		let elem = document.getElementById( this.props.prefix + "imagex" );
		if(elem)
			elem.setAttribute("src",this.state.url);
		
	}
	render()
	{
		return this.form1();	
	}
	form1()
	{
		const { id, prefix, url, bg } = this.state;
		const ext = url.substring( url.lastIndexOf(".") + 1 );
		const { height, padding } = this.props;
		const _height = height ? height : 70;
		const _padding = padding ? padding : 20;
		const bstyle = { height:_height + _padding*2, margin:"3px 3px 3px 0", minWidth:_height + _padding * 2, backgroundColor:this.props.bg, padding:_padding };
		//console.log(bstyle);
		const istyle 	= { 
			position:"relative", 
			display: "inline-flex", 
			justifyContent: "center", 
			alignItems: "center", 
			minWidth:_height, 
			height:_height, 
			overflow:"hidden"
		};
		const delbtn 	= url != "" && url != undefined ? <div 
					className="btn btn-link"
					style={{ alignSelf: "start", padding: "3px 6px", marginTop: 4, lineHeight: "3px"}} 
					onClick={this.onClear}
				>
					<i className="fas fa-times" />
				</div> : null;
		const cont = in_array( ext, ["jpg", "gif", "svg", "png", "bmp"] ) || url.indexOf("data:image/") != -1
			? <img 
				height={_height} 
				id={this.props.prefix + "imagex"} 
				src={url} 
				alt="" 
				style={{height:_height}}
			/>
			: <div>
				<div className={'fi fi-' + ext + ' fi-size-xs'}>
					<div className='fi-content'>{ext}</div>
				</div>
			</div>;
		const descr = this.props.isDescr ? 
				<span className="media-chooser-descr"  style={{padding:20}}>
					{this.state.name}
				</span> : null;
		return (
			<div className="media-chooser-cont" style={{display: "flex", flexDirection: "row", alignItems: "center"}}>
				<div className='media_button my_image_upload' style={bstyle} image_id={id}  prefix={prefix}>
					<div className='pictogramm ' id={prefix + id.toString()} style={istyle}>
						{url ? cont : null}
						<input type="file" 
							name="image_input_name" 
							style={{opacity:0, width:"100%", height:"100%", position:"absolute", top:0, left:0}}
							onChange={this.onImageChange}
						/>
					</div>				
				</div>
				{ delbtn }
				{ descr }
			</div>
		);
	}
	onClear = () =>
	{
		this.setState({url:"", id:-1});
		this.props.onChange( "", -1, this.props.ID );
	}
	onImageChange = (evt) =>
	{
		const _height = this.state.height ? this.state.height : 70;
		var file	= evt.target.files[0];
		if(!file)	return;
		if( $("#" + this.props.prefix + "imagex").length )
			$("#" + this.props.prefix + "imagex").detach();
		let elem = document.getElementById("#" + this.props.prefix + "imagex");
		if(elem)
		{
			elem.parentNode.removeChild(elem);
		}
		/**/
		var img 	= document.createElement("img");
		img.height	= _height;
		img.id 		= this.props.prefix + 'imagex';
		img.style	= "height:" + _height + "px";
		img.alt 	= '';
		img.file 	= file;
		img.files	= evt.target.files;
		//evt.target.parentElement.appendChild(img);
		var reader = new FileReader();
		reader.g = this;
		reader.onload = (function(aImg) 
		{ 
			return function(e) 
			{ 
				aImg.src = e.target.result; 
				reader.g.setState({url:aImg.src, name:aImg.file.name });
				reader.g.props.onChange( aImg.src, aImg.file, reader.g.props.ID );
			}; 
		})(img);
		reader.readAsDataURL(file);
		
		//
	}
	
	
	form2()
	{
		return (
			<div className="imageupload panel panel-default">
				<div className="panel-heading hidden  ">
					<h3 className="panel-title pull-left">
						{__("Upload Image")} />
					</h3>
					<div className="btn-group pull-right">
						<button type="button" className="btn btn-default active">
							{__("File")} />
						</button>
						<button type="button" className="btn btn-default">
							URL
						</button>
					</div>
				</div>
				<div className="file-tab panel-body">
					<label className="btn btn-link btn-file">
						<span>{__("Browse")} /></span>
						<input type="file" name="$image_input_name" style={{opacity:0}}/>
					</label>
					<button type="button" className="btn btn-link">{__("Remove")} /></button>
					<button type="button" className="btn btn-link">{__("Insert")} /></button>
				</div>
				<div className="url-tab panel-body hidden">
					<div className="input-group">
						<input type="text" className="form-control hasclear" placeholder="Image URL" />
						<div className="input-group-btn">
							<button type="button" className="btn btn-default">
								{__("Submit")}
							</button>
						</div>
					</div>
					<button type="button" className="btn btn-default ">
						{__("Remove")}
					</button>
					<input type="hidden" name="image-url" />
				</div>
				<div className="panel-heading clearfix  hidden">
					<div className="btn-group pull-right">
						<button type="button" className="btn btn-default">
							{__("Submit")}
						</button>
					</div>
				</div>
			</div>
			
		);
	}
}