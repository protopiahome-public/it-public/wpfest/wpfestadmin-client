import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


export default class MyEditor extends Component 
{
	constructor(props)
	{
		super(props);
		const html = this.props.text ? this.props.text : "";
		console.log(html);
		const contentBlock = htmlToDraft(html);
		if (contentBlock) {
		  const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
		  const editorState = EditorState.createWithContent(contentState);
		  this.state = {
			editorState,
			text:contentBlock
		  };
		}
	}

	onEditorStateChange: Function = (editorState) => 
	{
		this.setState({ editorState });
		this.props.onChange( draftToHtml(convertToRaw(editorState.getCurrentContent())) );
	};

	render() 
	{
		const { editorState } = this.state;
		return (
			<div>
				<Editor
					editorState={editorState}
					wrapperClassName="demo-wrapper"
					editorClassName="demo-editor"
					onEditorStateChange={this.onEditorStateChange}
				/>
			</div>
		);
	}
}