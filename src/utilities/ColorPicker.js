import React, {Component, Fragment} from "react"; 
import { ChromePicker } from 'react-color';

export default class ColorPicker extends Component
{
	constructor(props)
	{
		super(props);
		this.state = {
			color:this.props.color || "#FFFFFF",
			isColorPicker: false
		}
	}
	
	onColor = color =>
	{
		this.setState({ color: color.hex });
		this.props.onChoose( color );
	}
	onColorToggle = () =>
	{
		this.setState({isColorPicker:!this.state.isColorPicker});
	}
	render()
	{
		const picker = this.state.isColorPicker ? 
		<div style={{ position:"absolute", zIndex:20 }} onClick={this.onColorToggle} >
			<ChromePicker 
				disableAlpha = {true} 
				color={ this.state.color } 				
				onChange={this.onColor}
			/>
		</div>
		: null;
		return <Fragment>
			<div 
				style={{
					padding: '5px',
					background: '#fff',
					borderRadius: '1px',
					boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
					display: 'inline-block',
					cursor: 'pointer'
				 }}
			>
				<div 
					style={{ 
						width: '36px',
						height: '14px',
						borderRadius: '2px', 
						backgroundColor:this.state.color
					}} 
					onClick={this.onColorToggle}
				/>
			</div>
			{picker}
		</Fragment>
	 }
}