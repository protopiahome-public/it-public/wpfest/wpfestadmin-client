import voc from "./wpfa-ru_RU.json"; 
export function __(text)
{
	return  voc[text] ? voc[text] : text;
}