import React, { Component } from 'react';
import { __ } from "./utilities/Voc";
import { get_status, is_role, setCookie } from "./utilities/utils";
import { AppToaster } from "./utilities/blueUtils";
import HomeTab from "./components/HomeTab";
import RegistrationForm from "./components/RegistrationForm";
import PatternSiteTab from "./components/PatternSiteTab";
import AllSiteTab from "./components/AllSiteTab";
import VKLogin from './utilities/VKLogin';
import User from "./User";
import "./style.scss";

import { 
	Tab, Tabs, 
	Button, 
	Card, 
	Elevation,    
	Icon,
    InputGroup,
    Intent,
	FormGroup
} from "@blueprintjs/core";
import _fetch from "./api";
import { FacebookLoginButton } from "react-social-login-buttons";

class App extends Component 
{
	constructor(props)
	{
		super(props);
		this.state = {
			password:"",
			login:"",
			display_name:"",
			sites:[],
			all_sites:[],
			current:"hm0",
			isLoggedIn:0,
			collapsed:true
		};
	}
	componentWillMount()
	{
		//if(get_status() == "local") return;
		//console.log( window.login, window.password );
		//this.onLoginResult();
	}
	render()
	{
		let cont;
		switch(this.state.isLoggedIn)
		{
			case 1:
				cont = this.isRegisterIn();
				break;
			case 2:
				cont = this.isLoggrdIn();
				break;
			case 0:
			default:
				cont = this.isNotLoggedInn();
		}
		return <section className="m">
			<header/>
			<main>
				{cont}
			</main>
			<footer/>
		</section>;
	}
	isNotLoggedInn()
	{
		return <div className="d-flex justify-content-center align-items-center node">
			<Card interactive={true} elevation={Elevation.TWO} style={{maxWidth:400}}>
				<h5>{__("Authtorization")}</h5>
				<div>
					<InputGroup
						type="text"
						large={true}
						leftIcon="person"
						onChange={this.onLogin}
						value={this.state.login}
						className="mb-4"
					/>
					<InputGroup
						type="password"
						large={true}
						leftIcon="key"
						onChange={this.onPassword}
						value={this.state.password}
						className="mb-4"
					/>
				</div>
				<div className="mb-2">
					<VKLogin
						apiId="4059926"
						value={"VK"}
						fields="name,email,picture"
						onClick={this.onVK}
						callback={this.responseVk} 
					/>
					<div
						style={{ backgroundColor:"#EEE", width:100 }}
						className="soc"
						onClick={this.onAuth}
					>
						<div className="butn">{__("Enter")}</div>
					</div>
					<div 
						style={{ backgroundColor:"#EEE", width:100 }}
						className="soc" 
						onClick={this.onRegister}
					>
						<div className="butn">{__("Registration")}</div>
					</div>					
				</div>
				<div className="mb-2 hidden">
					
				</div>
			</Card>
		</div>
	}
	isLoggrdIn() 
	{
		//console.log(this.state.sites);
		return  <div className={this.state.collapsed ? "container m" : "container-fluid m"} >
				<div className="display-4 my-5 hidden">
					{ __("Settings") }
				</div>
				<div className="row m">
					<div className="col-md-12 m">
						<div className="mt-1" style={{ position:"absolute", right:15 }}>
							<Button minimal={true} rightIcon="log-out" onClick={this.logout}>
								{this.state.display_name} 
							</Button>
							<Button minimal={true} onClick={ ()=> this.setState({ collapsed: !this.state.collapsed }) }>
								<Icon icon={this.state.collapsed ? "zoom-to-fit" : "minimize"} />
							</Button>
						</div>
						<Tabs
							animate={true}
							id="TabsExample"
							className="node"
							large={true}
							onChange={this.onTab}
							className="m"
						>
							<Tab id="hm" title={ __("Home")} panel={ <HomeTab /> } />
							{
								is_role("administrator")
								?
								<Tab 	
									id="ps" 
									className="node"	
									title={ __("Pattern sites")} 
									panel={ <PatternSiteTab 
										current={this.state.current}
										sites={this.state.sites || []}
										addTab={this.addTab}
										onTitle={this.onTitle}
										removeTab={this.removeTab}
										onSave={this.onSave}
									/> } 
								/>
								:
								null
							}
							<Tab 
								id="as" 
								title={ __("All sites")} 
								className="node"
								panel={ <AllSiteTab
									onStart={this.onAllSites}
									onPagi={this.onAllSites}
									sites={this.state.sites || []}
									all_sites={this.state.all_sites || []}
									all_sites_count={this.state.all_sites_count}
									all_sites_offset={this.state.all_sites_offset}
									all_sites_number={this.state.all_sites_number}
								/> } 
							/>
						</Tabs>
					</div>
				</div>
			</div>;
	}
	isRegisterIn()
	{	
		return <RegistrationForm 
			onLogout={this.logout}
			onRegister={this.onRegisterResault}
		/>
	}
	onTab = (newTabId, prevTabId, evt) =>
	{
		switch( newTabId )
		{
			case "as":
				this.onAllSites(0);
				break;
			case "ps":
				
				break;
		}
	}
	onLogin = evt =>
	{
		//console.log(evt.currentTarget.value);
		this.setState({login:evt.currentTarget.value})
	}
	logout = () =>
	{
		this.setState({login:"", password:"", display_name: "", isLoggedIn:0 })
		User.deleteData( );
	}
	onPassword = evt =>
	{
		//console.log(evt.currentTarget.value);
		this.setState({password:evt.currentTarget.value})
	}
	onAuth = () =>
	{
		_fetch("auth", {login:this.state.login, password:this.state.password})
			.then(data =>
			{
				console.log(data);
				this.onlogged(data);
			});
	}
	onlogged = data =>
	{
		if(data.msg)
		{
			AppToaster.show({  
				intent: Intent.SUCCESS,
				icon: "person", 
				timeout:10000,
				className: "px-4 py-4",
				message: data.msg				
			});
		}
		if(data.is_login)
		{
			window.atoken = data.token;
			//setCookie("token", data.token, { expires : 4*3600 } );
			this.setState({display_name:data.user.display_name});
			User.setData(data.user);
			this.onLoginResult();
		}
	}
	onRegister = () =>
	{
		this.setState({ isLoggedIn : 1, login:"", password:"", display_name: "",  });
	}
	onRegisterResault = data =>
	{
		console.log(data);
		_fetch(
			"register_user",
			{
				login		: data.lgn,
				password	: data.psw,
				first_name	: data.first_name,
				last_name	: data.last_name
			}
		)
				.then(data => 
				{					
					console.log(data);
					this.onlogged(data);
				});
	}
	onLoginResult = () =>
	{
		setTimeout(()=>
		{
			_fetch("init")
				.then(data => 
				{
					//console.log(data);
					this.setState({sites:data.sites || [], isLoggedIn:2});
				});
		}, 600);
		
	}
	addTab = (id, tab) =>
	{
		let sites = this.state.sites.slice(0);
		sites.push({title:"New " + sites.length, id: ""+sites.length, domain:"", content:"" });
		this.setState({ sites, current:"" + (sites.length - 1) });
	}
	onTitle = (title, num, field) =>
	{
		let sites = this.state.sites.slice(0);
		sites[num][field] = title;
		this.setState({sites});
	}
	removeTab = (num) =>
	{
		let sites = this.state.sites.slice(0);
		sites.splice(num, 1);
		this.setState({sites});
	}
	onSave = () =>
	{
		//console.log(this.state.sites);
		_fetch( "save_sites", this.state.sites )
			.then(data => 
			{
				console.log(data);
				if(data.msg)
				{
					AppToaster.show({  
						intent: Intent.SUCCESS,
						icon: "floppy-disk", 
						timeout:10000,
						className: "px-4 py-4",
						message: data.msg				
					});
				}
			});
	}
	onAllSites = offset =>
	{
		//console.log( offset );
		_fetch( "get_all_sites", {offset:offset, number:10} )
			.then(data => 
			{
				console.log(data);
				this.setState({all_sites: data.sites || [], all_sites_number: data.number, all_sites_count: data.count, all_sites_offset: data.offset});
			})
	}
	onVK = evt =>
	{
		console.log( "onVK" );
	}
	responseVk = response =>
	{
		let res = response[0]
		res.oauth_type = "vk";
		console.log(res);
		_fetch( "vk_auth", res )
			.then(data => 
			{
				console.log(data);
				if(data.msg)
				{
					AppToaster.show({  
						intent: Intent.SUCCESS,
						icon: "person", 
						timeout:10000,
						className: "px-4 py-4",
						message: data.msg				
					});
				}
				if(data.is_login)
				{
					window.atoken = data.token;
					//setCookie("token", data.token, { expires : 4*3600 } );
					this.onLoginResult();					
					this.setState({display_name:data.user.display_name});
					User.setData(data.user);
				}
			});
	}
}

export default App;
